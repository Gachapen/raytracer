extern crate image;

mod math;

use std::f32::consts::PI;
use std::f32;
use std::fs::File;
use std::path::Path;

use image::ImageBuffer;
use image::ImageLuma8;

use math::vec::{cross, distance, dot, Vec2u, Vec3};

fn deg_to_rad(deg: f32) -> f32 {
    (deg / 180.0) * PI
}

struct Ray {
    origin: Vec3,
    direction: Vec3,
}

#[derive(PartialEq)]
enum Shape {
    Sphere,
    Rect,
    None,
}

trait Geometry {
    fn shape() -> Shape;
    fn normal(&self, surface_point: Vec3) -> Vec3;
    fn intersections(&self, ray: &Ray) -> Vec<Vec3>;

    fn intersects(&self, ray: &Ray) -> bool {
        !self.intersections(ray).is_empty()
    }
}

struct Material {
    opacity: f32,
    reflection: f32,
    refractive_index: f32,
}

impl Material {
    fn default() -> Material {
        Material {
            opacity: 1.0,
            reflection: 0.0,
            refractive_index: 1.0,
        }
    }
}

struct Sphere {
    center: Vec3,
    radius: f32,
}

impl Geometry for Sphere {
    fn shape() -> Shape {
        Shape::Sphere
    }

    fn normal(&self, surface_point: Vec3) -> Vec3 {
        (surface_point - self.center).normalized()
    }

    fn intersections(&self, ray: &Ray) -> Vec<Vec3> {
        let ray_to_center = self.center - ray.origin;
        let v = dot(ray_to_center, ray.direction);
        if v < 0.0 {
            return vec![];
        }

        let disc = self.radius.powi(2) - (dot(ray_to_center, ray_to_center) - v.powi(2));

        if disc < 0.0 {
            return vec![];
        } else {
            let d = disc.sqrt();
            return vec![
                ray.origin + ray.direction * (v - d),
                ray.origin + ray.direction * (v + d),
            ];
        }
    }
}

struct Rect {
    bot_left: Vec3,
    bot_right: Vec3,
    top_left: Vec3,
}

impl Rect {
    fn new(width: f32, height: f32, position: Vec3, rotation: Rotation) -> Rect {
        let half_width = width * 0.5;
        let half_height = height * 0.5;

        let bot_left = Vec3 {
            x: -half_width,
            y: 0.0,
            z: -half_height,
        };

        let bot_right = Vec3 {
            x: half_width,
            y: 0.0,
            z: -half_height,
        };

        let top_left = Vec3 {
            x: -half_width,
            y: 0.0,
            z: half_height,
        };

        Rect {
            bot_left: rotation.rotate(bot_left) + position,
            bot_right: rotation.rotate(bot_right) + position,
            top_left: rotation.rotate(top_left) + position,
        }
    }
}

impl Geometry for Rect {
    fn shape() -> Shape {
        Shape::Rect
    }

    fn normal(&self, _: Vec3) -> Vec3 {
        cross(
            self.top_left - self.bot_left,
            self.bot_right - self.bot_left,
        ).normalized()
    }

    fn intersections(&self, ray: &Ray) -> Vec<Vec3> {
        let left = self.top_left - self.bot_left;
        let bottom = self.bot_right - self.bot_left;

        let normal = cross(left, bottom);
        let nd = dot(normal, ray.direction);

        if nd.abs() < 1e-6 {
            return vec![];
        }

        let t = dot(-normal, (ray.origin - self.bot_left)) / nd;
        if t < 0.0 {
            return vec![];
        }

        let point = ray.origin + ray.direction * t;

        let u = dot(point - self.bot_left, left);
        if u < 0.0 || u > dot(left, left) {
            return vec![];
        }

        let v = dot(point - self.bot_left, bottom);
        if v < 0.0 || v > dot(bottom, bottom) {
            return vec![];
        }

        return vec![point];
    }
}

struct Rotation {
    euler: Vec3,
}

impl Rotation {
    fn from_euler(euler: Vec3) -> Rotation {
        Rotation { euler: euler }
    }

    fn from_direction(direction: Vec3) -> Rotation {
        Rotation {
            euler: Vec3 {
                x: direction.y.asin(),
                y: (-direction.x).atan2(direction.z),
                z: 0.0,
            },
        }
    }

    fn direction(&self) -> Vec3 {
        Vec3 {
            x: -self.euler.y.sin() * self.euler.x.cos(),
            y: self.euler.x.sin(),
            z: self.euler.y.cos() * self.euler.x.cos(),
        }
    }

    fn rotate(&self, vec: Vec3) -> Vec3 {
        let cos_y = self.euler.y.cos();
        let sin_y = self.euler.y.sin();
        let cos_x = self.euler.x.cos();
        let sin_x = self.euler.x.sin();

        Vec3 {
            x: cos_y * vec.x + sin_y * sin_x * vec.y + sin_y * cos_x * vec.z,
            y: cos_x * vec.y - sin_x * vec.z,
            z: -sin_y * vec.x + cos_y * sin_x * vec.y + cos_y * cos_x * vec.z,
        }
    }
}

struct Camera {
    position: Vec3,
    rotation: Rotation,
    size: Vec2u,
    fov: f32,
}

impl Camera {
    #[allow(dead_code)]
    fn new() -> Camera {
        Camera {
            position: Vec3::zero(),
            rotation: Rotation::from_direction(Vec3::forward()),
            size: Vec2u { x: 100, y: 100 },
            fov: deg_to_rad(90.0),
        }
    }

    #[allow(dead_code)]
    fn ray(&self) -> Ray {
        Ray {
            origin: self.position,
            direction: self.rotation.direction(),
        }
    }

    fn ray_through(&self, x: u32, y: u32) -> Ray {
        let aspect = self.size.y as f32 / self.size.x as f32;
        let y_fov = self.fov * aspect;

        let x_pixel_width = self.fov / self.size.x as f32;
        let y_pixel_width = y_fov / self.size.y as f32;

        let angle_x = x_pixel_width * 0.5 + x as f32 * x_pixel_width - self.fov * 0.5;
        let angle_y = y_pixel_width * 0.5 + y as f32 * y_pixel_width - y_fov * 0.5;

        Ray {
            origin: self.position,
            direction: Rotation::from_euler(Vec3 {
                y: self.rotation.euler.y - angle_x,
                x: self.rotation.euler.x - angle_y,
                z: 0.0,
            }).direction(),
        }
    }
}

struct Texture {
    width: u32,
    height: u32,
    pixels: Vec<u8>,
}

impl Texture {
    fn new(width: u32, height: u32) -> Texture {
        Texture {
            width: width,
            height: height,
            pixels: vec![128; (width * height) as usize],
        }
    }
}

#[allow(dead_code)]
fn draw_term(surface: &Texture) {
    const TERM_CHARS: [char; 10] = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@'];

    for (i, val) in surface.pixels.iter().enumerate() {
        if i % surface.width as usize == 0 {
            println!("");
        }

        let term_index = (f32::from(*val) * (TERM_CHARS.len() as f32 / 256.0)) as usize;
        print!("{}", TERM_CHARS[term_index]);
    }

    println!("");
}

fn draw_png(surface: &Texture) {
    let mut imgbuf = ImageBuffer::new(surface.width, surface.height);

    for (i, val) in surface.pixels.iter().enumerate() {
        let x = i as u32 % surface.width;
        let y = i as u32 / surface.width;
        imgbuf.put_pixel(x, y, image::Luma([*val]));
    }

    let mut file = File::create(&Path::new("output.png")).unwrap();
    ImageLuma8(imgbuf)
        .save(&mut file, image::PNG)
        .expect("Could not save PNG");
}

struct Hit {
    distance: f32,
    point: Vec3,
    normal: Vec3,
    shape: Shape,
    index: usize,
    object_id: usize,
}

struct ObjectGeometry<G: Geometry> {
    geometry: G,
    object_id: usize,
}

fn find_closest_intersection<T: Geometry>(
    ray: &Ray,
    shapes: &[ObjectGeometry<T>],
    closest: &mut Hit,
) {
    for (i, shape) in shapes.iter().enumerate() {
        let points = shape.geometry.intersections(ray);
        for point in points {
            let distance = distance(point, ray.origin);
            if distance < closest.distance && distance > 0.01 {
                closest.distance = distance;
                closest.point = point;
                closest.normal = shape.geometry.normal(point);
                closest.shape = T::shape();
                closest.index = i;
                closest.object_id = shape.object_id;
            }
        }
    }
}

fn find_shadow<T: Geometry>(
    hit: &Hit,
    light_ray: &Ray,
    light_distance: f32,
    shapes: &[ObjectGeometry<T>],
    shadow: &mut bool,
) {
    'shapes: for (i, shape) in shapes.iter().enumerate() {
        if hit.shape == T::shape() && hit.index == i {
            continue;
        }

        let points = shape.geometry.intersections(light_ray);
        for point in points {
            if distance(hit.point, point) < light_distance {
                *shadow = true;
                break 'shapes;
            }
        }
    }
}

fn main() {
    struct Object {
        material: Material,
    }

    struct Scene {
        objects: Vec<Object>,
        spheres: Vec<ObjectGeometry<Sphere>>,
        rects: Vec<ObjectGeometry<Rect>>,
        light: Vec3,
        camera: Camera,
        default_shade: f32,
    }

    impl Scene {
        fn register_object(&mut self, material: Material) -> usize {
            let obj = Object { material: material };

            let id = self.objects.len();
            self.objects.push(obj);

            id
        }

        fn add_sphere(&mut self, geometry: Sphere, material: Material) -> usize {
            let id = self.register_object(material);
            self.spheres.push(ObjectGeometry {
                geometry: geometry,
                object_id: id,
            });
            id
        }

        fn add_rect(&mut self, geometry: Rect, material: Material) -> usize {
            let id = self.register_object(material);
            self.rects.push(ObjectGeometry {
                geometry: geometry,
                object_id: id,
            });
            id
        }
    }

    let mut scene = Scene {
        objects: vec![],
        spheres: vec![],
        rects: vec![],
        light: Vec3 {
            x: -3.0,
            y: 5.0,
            z: -8.0,
        },
        camera: Camera {
            size: Vec2u { x: 1000, y: 1000 },
            fov: deg_to_rad(90.0),
            position: Vec3 {
                z: -10.0,
                ..Vec3::zero()
            },
            rotation: Rotation::from_direction(Vec3::forward()),
        },
        default_shade: 0.3,
    };

    println!("Camera has angles {:?}", scene.camera.rotation.euler);
    println!(
        "Camera is looking in direction {:?}",
        scene.camera.rotation.direction()
    );

    scene.add_sphere(
        Sphere {
            radius: 3.0,
            center: Vec3::zero(),
        },
        Material::default(),
    );
    scene.add_sphere(
        Sphere {
            radius: 3.0,
            center: Vec3 {
                x: 5.0,
                y: 5.0,
                z: 3.0,
            },
        },
        Material {
            reflection: 0.5,
            ..Material::default()
        },
    );
    scene.add_sphere(
        Sphere {
            radius: 5.0,
            center: Vec3 {
                x: -5.0,
                y: -3.0,
                z: 1.0,
            },
        },
        Material {
            reflection: 0.0,
            ..Material::default()
        },
    );
    scene.add_sphere(
        Sphere {
            radius: 1.0,
            center: Vec3 {
                x: -4.0,
                y: 3.0,
                z: -5.0,
            },
        },
        Material::default(),
    );
    scene.add_sphere(
        Sphere {
            radius: 2.0,
            center: Vec3 {
                x: 1.0,
                y: -1.0,
                z: -6.0,
            },
        },
        Material {
            opacity: 0.2,
            refractive_index: 1.1,
            ..Material::default()
        },
    );

    scene.add_rect(
        Rect::new(
            20.0,
            20.0,
            Vec3 {
                y: -10.0,
                ..Vec3::zero()
            },
            Rotation::from_euler(Vec3::zero()),
        ),
        Material::default(),
    );

    let mut surface = Texture::new(scene.camera.size.x, scene.camera.size.y);

    for y in 0..scene.camera.size.y {
        for x in 0..scene.camera.size.x {
            #[derive(Copy, Clone)]
            struct TraceState {
                refractive_index: f32,
                depth: u32,
            }

            fn trace_ray(ray: &Ray, scene: &Scene, state: TraceState) -> f32 {
                let mut closest_hit = Hit {
                    distance: f32::INFINITY,
                    point: Vec3::zero(),
                    normal: Vec3::zero(),
                    shape: Shape::None,
                    index: 0,
                    object_id: 0,
                };

                find_closest_intersection(ray, &scene.spheres, &mut closest_hit);
                find_closest_intersection(ray, &scene.rects, &mut closest_hit);

                if closest_hit.distance != f32::INFINITY {
                    let light_dir = (scene.light - closest_hit.point).normalized();
                    let light_ray = Ray {
                        origin: closest_hit.point,
                        direction: light_dir,
                    };
                    let light_distance = distance(scene.light, closest_hit.point);

                    let mut shadow = false;

                    find_shadow(
                        &closest_hit,
                        &light_ray,
                        light_distance,
                        &scene.spheres,
                        &mut shadow,
                    );
                    find_shadow(
                        &closest_hit,
                        &light_ray,
                        light_distance,
                        &scene.rects,
                        &mut shadow,
                    );

                    let point_shade = {
                        if shadow {
                            0.0
                        } else {
                            let shade = dot(light_dir, closest_hit.normal);
                            if shade < 0.0 {
                                0.0
                            } else {
                                shade
                            }
                        }
                    };

                    let material = &scene.objects[closest_hit.object_id].material;
                    let mut reflect_shade = 0.0;
                    let mut refract_shade = 0.0;

                    let transparency = 1.0 - material.opacity;
                    let reflection = material.reflection - (transparency * material.reflection);
                    let diffuse = 1.0 - transparency - reflection;

                    if state.depth < 10 {
                        let (normal, c1, entering) = {
                            let dot = dot(closest_hit.normal, ray.direction);
                            if dot > 0.0 {
                                (-closest_hit.normal, dot, false)
                            } else {
                                (closest_hit.normal, -dot, true)
                            }
                        };

                        if reflection > 0.0 && entering {
                            let reflected_ray = Ray {
                                origin: closest_hit.point,
                                direction: ray.direction +
                                    (closest_hit.normal * 2.0 *
                                        -dot(closest_hit.normal, ray.direction)),
                            };

                            let new_state = TraceState {
                                depth: state.depth + 1,
                                ..state
                            };
                            reflect_shade = trace_ray(&reflected_ray, scene, new_state);
                        }

                        if transparency < 1.0 {
                            let n = state.refractive_index / material.refractive_index;

                            let cs2 = 1.0 - n.powi(2) * (1.0 - c1.powi(2));
                            if cs2 >= 0.0 {
                                let refracted_ray = Ray {
                                    origin: closest_hit.point,
                                    direction: ray.direction * n + normal * (n * c1 - cs2.sqrt()),
                                };

                                let new_state = TraceState {
                                    refractive_index: material.refractive_index,
                                    depth: state.depth + 1,
                                };
                                refract_shade = trace_ray(&refracted_ray, scene, new_state);
                            }
                        }
                    } else {
                        println!("Max depth reached ({})", state.depth);
                    }

                    point_shade * diffuse + reflect_shade * reflection +
                        refract_shade * transparency
                } else {
                    scene.default_shade
                }
            }

            let state = TraceState {
                refractive_index: 1.0,
                depth: 0,
            };

            let shade = trace_ray(&scene.camera.ray_through(x, y), &scene, state);
            let screen_index = (y * scene.camera.size.x + x) as usize;
            surface.pixels[screen_index] = (shade * 255.0) as u8;
        }
    }

    //draw_term(&surface);
    draw_png(&surface);
}
